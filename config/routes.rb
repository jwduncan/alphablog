Rails.application.routes.draw do
 root 'pages#home'
 get  'about', to: 'pages#about'
 
 resources :articles
 
 # users signing up 
 get 'signup', to: 'users#new'
 resources :users, except: [:new]

 # login to the system
 get    'login',  to: 'sessions#new'
 post   'login',  to: 'sessions#create'
 delete 'logout', to: 'sessions#destroy'
 
 resources :categories, except: [:destroy]
 
end
